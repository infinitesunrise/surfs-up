local entity_id = GetUpdatedEntityID()
local x, y = EntityGetTransform(entity_id)
local verlet_physics_component = EntityGetComponent(entity_id, "VerletPhysicsComponent")[1]
local positions = ComponentGetValue2(verlet_physics_component, "positions")
local waving_x = x + (math.sin(GameGetFrameNum() * 0.01) * 25)
positions[1] = x
positions[2] = y
positions[17] = waving_x
positions[18] = y - 256
ComponentSetValue2(verlet_physics_component, "positions", positions)