local entity_id = GetUpdatedEntityID()
local entity_x, entity_y = EntityGetTransform(entity_id)

local hit, hit_x, hit_y = RaytraceSurfaces(entity_x, entity_y, entity_x, entity_y + 512)
if hit then
    EntityApplyTransform(entity_id, hit_x, hit_y)
    state = STATE_SPAWN
else
    --ya missed, goodnight sweet prince
    EntityKill(entity_id)
end

EntityAddComponent2(entity_id, "SpriteAnimatorComponent", {})

local random = Random(1,10)
if random == 1 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/anemone.xml",
        offset_x = "7",
        offset_y = "12",  --sunken in 2 pixels
        rect_animation = "main",
    })
elseif random == 2 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/anemone2.xml",
        offset_x = "7",
        offset_y = "12",  --sunken in 2 pixels
        rect_animation = "main",
    })
elseif random == 3 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/anemone_small1.png",
        offset_x = "3",
        offset_y = "8"  --sunken in 1 pixels
    })
elseif random == 4 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/anemone_small2.png",
        offset_x = "3",
        offset_y = "8"  --sunken in 1 pixels
    })
elseif random == 5 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/anemone_small3.png",
        offset_x = "3",
        offset_y = "8"  --sunken in 1 pixels
    })
elseif random == 6 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/purple_coral.png",
        offset_x = "18",
        offset_y = "48"  --sunken in 2 pixels
    })
elseif random == 7 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/brain_coral1.png",
        offset_x = "14",
        offset_y = "12"  --sunken in 2 pixels
    })
elseif random == 8 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/brain_coral2.png",
        offset_x = "14",
        offset_y = "12"  --sunken in 2 pixels
    })
elseif random == 9 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/chimney_coral1.png",
        offset_x = "14",
        offset_y = "32"  --sunken in 2 pixels
    })
elseif random == 10 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/chimney_coral2.png",
        offset_x = "14",
        offset_y = "32"  --sunken in 2 pixels
    })
end

--just remove self completely to avoid interfereing with other scripts
local this_script = EntityGetComponent(entity_id, "LuaComponent")
EntityRemoveComponent(entity_id, this_script)