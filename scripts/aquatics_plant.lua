local entity_id = GetUpdatedEntityID()
local entity_x, entity_y = EntityGetTransform(entity_id)

local hit, hit_x, hit_y = RaytraceSurfaces(entity_x, entity_y, entity_x, entity_y + 512)
if hit then
    EntityApplyTransform(entity_id, hit_x, hit_y)
    entity_x, entity_y = EntityGetTransform(entity_id)
else
    --ya missed, goodnight sweet prince
    EntityKill(entity_id)
end

EntityAddComponent2(entity_id, "SpriteAnimatorComponent", {})

local max_random = 4
local spawn_big_kelp = ModSettingGet("water_world.spawn_big_kelp")
if spawn_big_kelp == nil then spawn_big_kelp = true end
if spawn_big_kelp then
    max_random = max_random + 1
end

local random = Random(1,max_random)
if random == 1 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/kelp.xml",
        offset_x = "18",
        offset_y = "115", --sunken in 1 pixel
        rect_animation = "main",
    })
elseif random == 2 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/kelp2.xml",
        offset_x = "11",
        offset_y = "67", --sunken in 2 pixels
        rect_animation = "main",
    })
elseif random == 3 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/brown_algae.xml",
        offset_x = "28",
        offset_y = "38", --sunken in 6 pixels
        rect_animation = "main",
    })
elseif random == 4 then
    EntityAddComponent(entity_id, "SpriteComponent", {
        image_file = "mods/water_world/files/seaflowers.xml",
        offset_x = "17",
        offset_y = "24",  --sunken in 8 pixels
        rect_animation = "main",
    })
elseif random == 5 then
    EntityLoad("mods/water_world/files/verlet_kelp.xml", entity_x, entity_y)
end

--just remove self completely to avoid interfereing with other scripts
local this_script = EntityGetComponent(entity_id, "LuaComponent")
EntityRemoveComponent(entity_id, this_script)