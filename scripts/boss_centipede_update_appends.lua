-- The boss can't die normally; if their HP is zero, this does stuff instead
function check_death()
	if is_dead then return end
	
	local entity_id = GetUpdatedEntityID()
	SetRandomSeed( GameGetFrameNum(), GameGetFrameNum() )

	local comp = EntityGetFirstComponent( entity_id, "DamageModelComponent" )
	if( comp ~= nil ) then
		local hp = ComponentGetValueFloat( comp, "hp" )

		-- check aggro
		if not is_aggro then
			local max_hp = ComponentGetValueInt( comp, "max_hp")
			is_aggro = hp <= clamp(max_hp * 0.1, 9, 40)
			--print("hp/max: " .. hp .. " / " .. max_hp)

			-- enter aggro mode
			if is_aggro then
				-- store aggro to entity
				EntityAddComponent( entity_id, "VariableStorageComponent", 
				{ 
					name = "aggro",
				})

				set_main_animation("aggro", "aggro")

				-- spawn body chunks
				GameEntityPlaySound( entity_id, "destroy_face" )
				local x,y = EntityGetTransform( entity_id )
				local o = EntityLoad( "data/entities/animals/boss_centipede/body_chunks.xml", x, y)
				PhysicsApplyForce( o, 0, -600)
				PhysicsApplyTorque( o, 200)
				GameCreateParticle( "slime_green", x+20, y, 250, 0, -20, true, false )
			
				phase_repeats = 0
				next_phase()
			end
		end

		-- check death
		if ( hp <= 0.0 ) then
			move_to_reference()
			GameTriggerMusicFadeOutAndDequeueAll()
			if death_sound_started == false then
				GameEntityPlaySound( GetUpdatedEntityID(), "dying" )
				death_sound_started = true
			end

			--MOD: drop projectile weakening spell on death
			print("dropping projectile weaking curse")
			local boss_x, boss_y = EntityGetTransform(entity_id)
			CreateItemActionEntity("curse_wither_projectile", boss_x, boss_y)

			local o = EntityLoad( "data/entities/animals/boss_centipede/body_chunks.xml", x, y)
				PhysicsApplyForce( o, 0, -600)
				PhysicsApplyTorque( o, 200)

			for i = 1,40 do
				local rand = function() return Random( -10, 10 ) end
				local x,y = EntityGetTransform( entity_id )
				GameScreenshake( i * 1, x, y )
				GameCreateParticle( "slime_green",            x + rand(), y + rand(), 10, i * 5.5, i * 5.5, true, false )
				if i > 20 then
					GameCreateParticle( "gunpowder_unstable", x + rand(), y + rand(), 3,  40.0,    40.0,    true, false )
				end

				wait( 3 )
			end
			
			local reference = EntityGetWithTag( "reference" )
			local x_portal,y_portal = EntityGetTransform( GetUpdatedEntityID() )
			
			if ( #reference > 0 ) then
				local ref_id = reference[1]
				x_portal,y_portal = EntityGetTransform( ref_id )
				EntityKill( ref_id )
			end

			-- fix for spider legs, looks visually quite bad (what? how's this related to spider legs)
			-- 150 puts it right under the bridge, which kinda cool
			y_portal = y_portal + 50
			
			EntityLoad( "data/entities/buildings/teleport_ending_victory_delay.xml", x_portal, y_portal )

			-- kill
			comp = EntityGetFirstComponent( GetUpdatedEntityID(), "DamageModelComponent" )
			if( comp ~= nil ) then
				ComponentSetValue( comp, "kill_now", "1" )
			end

			GlobalsSetValue( "FINAL_BOSS_ACTIVE", "0" )

			local boss_kill_count = tonumber( GlobalsGetValue( "GLOBAL_BOSS_KILL_COUNT", "0" ) )
			boss_kill_count = boss_kill_count + 1
			print( "boss_kill_count: " .. boss_kill_count )
			GlobalsSetValue( "GLOBAL_BOSS_KILL_COUNT", tostring( boss_kill_count ) )
			
			AddFlagPersistent( "boss_centipede" )
			StatsLogPlayerKill( GetUpdatedEntityID() ) -- this is needed because otherwise the boss kill doesn't get registered as a kill for the player
			is_dead = true

			return
		end
	end
end