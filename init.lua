ModLuaFileAppend("data/entities/animals/boss_centipede/boss_centipede_update.lua", "mods/water_world/scripts/boss_centipede_update_appends.lua")
ModRegisterAudioEventMappings("mods/water_world/files/audio_events.txt")

dofile_once("data/scripts/perks/perk.lua")
dofile_once("data/scripts/perks/perk_list.lua")

cheeky_flavor_text = {
    "take a deep breath...",
    "cowabunga, dude",
    "hang ten!",
    "20,000 leagues under the mountain",
    "jump in, the water's fine!"
}

holy_mountain_dimensions = {
    {
        min_x = -825,
        min_y = 1250,
        max_x = 250,
        max_y = 1475
    },
    {
        min_x = -825,
        min_y = 2800,
        max_x = 250,
        max_y = 3025
    },
    {
        min_x = -825,
        min_y = 4850,
        max_x = 250,
        max_y = 5075
    },
    {
        min_x = -825,
        min_y = 6400,
        max_x = 250,
        max_y = 6625
    },
    {
        min_x = -825,
        min_y = 8450,
        max_x = 250,
        max_y = 8675
    },
    {
        min_x = -825,
        min_y = 10500,
        max_x = 250,
        max_y = 10725
    },
    {
        min_x = 1750,
        min_y = 13050,
        max_x = 2700,
        max_y = 13275
    },
}

protect_holy_mountains = true
you_have_breathless = false
helper_perks = true
aquatics_density = 5
enemies_have_breathless = true
breathing_room = 10
spawner_count = 3

function OnPlayerSpawned(player)
    local player_x, player_y = EntityGetTransform(player)
    --option values
    protect_holy_mountains = ModSettingGet("water_world.protect_holy_mountains")
    if protect_holy_mountains == nil then protect_holy_mountains = true end
    you_have_breathless = ModSettingGet("water_world.you_have_breathless")
    if you_have_breathless == nil then you_have_breathless = false end
    helper_perks = ModSettingGet("water_world.helper_perks")
    if helper_perks == nil then helper_perks = true end
    aquatics_density = ModSettingGet("water_world.aquatics_density")
    if aquatics_density == nil then aquatics_density = 5 end
    enemies_have_breathless = ModSettingGet("water_world.enemies_have_breathless")
    if enemies_have_breathless == nil then enemies_have_breathless = true end
    breathing_room = ModSettingGet("water_world.breathing_room")
    if breathing_room == nil then breathing_room = 10 end
    spawner_count = ModSettingGet("water_world.spawner_count")
    if spawner_count == nil then spawner_count = 3 end

    --snorkel up
    EntityAddComponent(player, "SpriteComponent", {
        _tags="character,snorkel",
        alpha="1",
        image_file="data/enemies_gfx/snorkel.xml",
        next_rect_animation="",
        offset_x="6",
        offset_y="15", 
        rect_animation="walk",
        z_index="0.58"
    })

    --spawn greed perk at start
    if helper_perks then
        local helper_perks_spawned = tonumber(GlobalsGetValue("water_world.herlper_perks_spawned", "0"))
        if helper_perks_spawned == 0 then
            perk_spawn_with_name(475, -115, "EXTRA_MONEY", true)
            perk_spawn_with_name(500, -115, "REMOVE_FOG_OF_WAR", true)
            GlobalsSetValue("water_world.herlper_perks_spawned", "1")
        end
    end

    --some silly aquatic flavor
    GamePlaySound("mods/water_world/files/water_world_audio.bank", "water_world/surfs_up", player_x, player_y)
    local cheeky_index = Random(1, #cheeky_flavor_text)
    GamePrintImportant("SURF'S UP!", cheeky_flavor_text[cheeky_index], "data/ui_gfx/decorations/3piece_water_msg.png")

    --give breathless if enabled in settings
    if you_have_breathless then
        local perk = perk_spawn(player_x, player_y, "BREATH_UNDERWATER")
        perk_pickup(perk, player, EntityGetName(perk), false, false)
    end
end

function OnWorldPreUpdate()
    local last_sample_frame = tonumber(GlobalsGetValue("water_world.last_sample_frame","0"))
    local this_frame = GameGetFrameNum()

    if this_frame - last_sample_frame >= 5 then
        local player = EntityGetWithTag("player_unit")[1]
        if player == nil then
            return
        end
        local player_x, player_y = EntityGetTransform(player)

        if GameGetFrameNum() > 300 then
             --spawn blobs of sea in open air
            spawn_sea_nearby(player_x, player_y, 384, spawner_count, breathing_room)

            --spawn aquatic props and animals
            spawn_aquatics_nearby(player_x, player_y, aquatics_density, 384, spawner_count)
        end

        --if enemies with breathless is enabled in settings
        if enemies_have_breathless then
            enforce_enemy_breathless(player_x, player_y, 512)
        end

        GlobalsSetValue("water_world.last_sample_frame", tostring(this_frame))
    end
end

function random_point_near_player(player_x, player_y, radius)
    local offset_x = Random(-radius, radius)
    local offset_y = Random(-radius, radius)
    local random_x = player_x + offset_x
    local random_y = player_y + offset_y
    local point = { x = random_x, y = random_y }
    return point
end

--attempts to spawn some sea at a point near an entity
function spawn_sea_nearby(player_x, player_y, radius, spawner_count, breathing_room)
    for _ = 0, spawner_count do
        local point = random_point_near_player(player_x, player_y, radius)
        if breathing_room ~= nil and point_is_allowed(point) then
            local checker_xml = "data/entities/misc/air_material_checker.xml"
            local checker_entity = EntityLoad(checker_xml, point.x, point.y)
            local material_checker = EntityGetComponent(checker_entity, "MaterialAreaCheckerComponent")[1]
            local min_x, min_y, max_x, max_y = ComponentGetValue2(material_checker, "area_aabb")
            min_x = min_x * breathing_room
            min_y = min_y * breathing_room
            max_x = max_x * breathing_room
            max_y = max_y * breathing_room
            ComponentSetValue2(material_checker, "area_aabb", min_x, min_y, max_x, max_y)
        end
    end
end

function spawn_aquatics_nearby(player_x, player_y, max_count, radius, spawner_count)
    --plants
    local plants = EntityGetInRadiusWithTag(player_x, player_y, radius, "aquatics_plant")
    if #plants < max_count then
        for _ = 0, spawner_count do
            local point = random_point_near_player(player_x, player_y, radius)
            if point_is_allowed(point) then
                EntityLoad("data/entities/vegetation/aquatics_plant.xml", point.x, point.y)
            end
        end
    end

    --corals
    local corals = EntityGetInRadiusWithTag(player_x, player_y, radius, "aquatics_coral")
    if #corals < max_count then
        for _ = 0, spawner_count do
            local point = random_point_near_player(player_x, player_y, radius)
            if point_is_allowed(point) then
                EntityLoad("data/entities/vegetation/aquatics_coral.xml", point.x, point.y)
            end
        end
    end

    --fishies
    local fishies = EntityGetInRadiusWithTag(player_x, player_y, radius, "aquatics_fishie")
    if #fishies < max_count then
        local point = random_point_near_player(player_x, player_y, radius)
        if point_is_allowed(point) then
            local random = Random(1,2)
            local fishie = nil
            if random == 1 then
                fishie = EntityLoad("data/entities/animals/fish.xml",point.x, point.y)
            elseif random == 2 then
                fishie = EntityLoad("data/entities/animals/fish_large.xml",point.x, point.y)
            end
            EntityAddTag(fishie, "aquatics_fishie")
        end
    end
end

function material_area_checker_success(x, y)
    local entity_id = GetUpdatedEntityID()
    local entity_x, entity_y = EntityGetTransform(entity_id)
    local sea_spawner = EntityLoad("data/entities/projectiles/deck/sea_spawner.xml", entity_x, entity_y)
    GameShootProjectile(0, x, y, x, y, sea_spawner)
    --goodnight sweet prince
    EntityKill(entity_id)
end


function point_is_allowed(point)
    local holy_mountains = {}
    if protect_holy_mountains then
        local holy_mountain_flags = {
            tonumber(GlobalsGetValue("TEMPLE_COLLAPSED_0_2", "0")),
            tonumber(GlobalsGetValue("TEMPLE_COLLAPSED_0_5", "0")),
            tonumber(GlobalsGetValue("TEMPLE_COLLAPSED_0_9", "0")),
            tonumber(GlobalsGetValue("TEMPLE_COLLAPSED_0_12", "0")),
            tonumber(GlobalsGetValue("TEMPLE_COLLAPSED_0_16", "0")),
            tonumber(GlobalsGetValue("TEMPLE_COLLAPSED_0_20", "0")),
            tonumber(GlobalsGetValue("FINAL_BOSS_ARENA_ENTERED", "0"))
        }
        for index,flag in ipairs(holy_mountain_flags) do
            if flag == 0 then
                table.insert(holy_mountains, holy_mountain_dimensions[index])
            end
        end
        for _,dimensions in ipairs(holy_mountains) do
            if point_is_in_box(point, dimensions) then
                return false
            end
        end
    end
    return true
end

function point_is_in_box(point, box)
    if box.min_x < point.x
    and point.x < box.max_x
    and box.min_y < point.y
    and point.y < box.max_y then
        return true
    end
    return false
end

function enforce_enemy_breathless(x, y, radius)
    local enemy_ids = EntityGetInRadiusWithTag(x, y, radius, "enemy")
    for i = 1, #enemy_ids, 1 do
        local enemy_id = enemy_ids[i]
        if EntityHasTag(enemy_id, "has_breathless") == false then
            EntityAddTag(enemy_id, "has_breathless")
            if is_valid_enemy_for_perks(enemy_id) then
                local breathless = perk_list[2]
                local game_effect_comp = GetGameEffectLoadTo(enemy_id, "BREATH_UNDERWATER", true)
                if game_effect_comp ~= nil then
                    ComponentSetValue(game_effect_comp, "frames", "-1")
                end
                breathless.func(0, enemy_id)
            end
        end
    end
end

function is_valid_enemy_for_perks(enemy_id)
    if ( enemy_id == nil ) and ( enemy_id == NULL_ENTITY ) then
        return false
    end

    local enemy_filename = EntityGetFilename(enemy_id)
	local animals_path = "data/entities/animals/"
    local boss_path = "/boss_"
    local is_boss = string.find(enemy_filename, boss_path)
	if (string.sub(enemy_filename, 1, #animals_path) ~= animals_path)
    or (is_boss ~= nil) then
		return false
	end

    local worm = EntityGetComponent(target, "WormAIComponent")
    local dragon = EntityGetComponent(target, "BossDragonComponent")
    local ghost = EntityGetComponent(target, "GhostComponent")
    local lukki = EntityGetComponent(target, "LimbBossComponent")
	if (worm ~= nil) or (dragon ~= nil) or (ghost ~= nil) or (lukki ~= nil) then
        return false
    end
	
	return true
end