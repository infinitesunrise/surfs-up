function ModSettingsGuiCount() return 1 end
dofile_once("data/scripts/lib/utilities.lua")

local water_world_settings = {}

function ModSettingsUpdate(init_scope)
  local breathing_room = {
    id = "BREATHING_ROOM",
    type = "breathing_room",
    name = "Breathing room",
    key = "water_world.breathing_room"
  }
  table.insert(water_world_settings, breathing_room)
  if (ModSettingGet(breathing_room.key) == nil) then
    ModSettingSet(breathing_room.key, 10)
  end

  local aquatics_density = {
    id = "AQUATICS_DENSITY",
    type = "aquatics_density",
    name = "Aquatics density",
    key = "water_world.aquatics_density"
  }
  table.insert(water_world_settings, aquatics_density)
  if (ModSettingGet(aquatics_density.key) == nil) then
    ModSettingSet(aquatics_density.key, 5)
  end

  local spawn_big_kelp = {
    id = "SPAWN_BIG_KELP",
    type = "spawn_big_kelp",
    name = "Spawn big kelp",
    key = "water_world.spawn_big_kelp"
  }
  table.insert(water_world_settings, spawn_big_kelp)
  if (ModSettingGet(spawn_big_kelp.key) == nil) then
    ModSettingSet(spawn_big_kelp.key, true)
  end

  local protect_holy_mountains = {
    id = "PROTECT_HOLY_MOUNTAINS",
    type = "protect_holy_mountains",
    name = "Protect holy mountains",
    key = "water_world.protect_holy_mountains"
  }
  table.insert(water_world_settings, protect_holy_mountains)
  if (ModSettingGet(protect_holy_mountains.key) == nil) then
    ModSettingSet(protect_holy_mountains.key, true)
  end

  local you_have_breathless = {
    id = "YOU_HAVE_BREATHLESS",
    type = "you_have_breathless",
    name = "You have breathless",
    key = "water_world.you_have_breathless"
  }
  table.insert(water_world_settings, you_have_breathless)
  if (ModSettingGet(you_have_breathless.key) == nil) then
    ModSettingSet(you_have_breathless.key, false)
  end

  local enemies_have_breathless = {
    id = "ENEMIES_HAVE_BREATHLESS",
    type = "enemies_have_breathless",
    name = "Enemies have breathless",
    key = "water_world.enemies_have_breathless"
  }
  table.insert(water_world_settings, enemies_have_breathless)
  if (ModSettingGet(enemies_have_breathless.key) == nil) then
    ModSettingSet(enemies_have_breathless.key, true)
  end

  local helper_perks = {
    id = "HELPER_PERKS",
    type = "helper_perks",
    name = "Helper perks",
    key = "water_world.helper_perks"
  }
  table.insert(water_world_settings, helper_perks)
  if (ModSettingGet(helper_perks.key) == nil) then
    ModSettingSet(helper_perks.key, true)
  end
end

function ModSettingsGui(gui, in_main_menu)
  local _id = 0
  local function id()
    _id = _id + 1
    return _id
  end

  GuiOptionsAdd(gui, GUI_OPTION.DrawActiveWidgetCursorOnBothSides)

  GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  GuiText(gui, 0, 0, "Scale the approximate size of air pockets")
  GuiLayoutBeginHorizontal(gui, 0, 0)
  local breathing_room = ModSettingGet("water_world.breathing_room")
  GuiText(gui, 0, 0, "   Breathing room:   ")
  if (breathing_room == nil) then
    breathing_room = 2
  end
  
  --GuiSlider( gui:obj, id:int, x:number, y:number, text:string, value:number, value_min:number, value_max:number, value_default:number, value_display_multiplier:number, value_formatting:string, width:number )
  local next_value = GuiSlider(gui, id(), -2, 0, "", breathing_room, 0, 25, 10, 1, " $0 ", 65)
  if(next_value ~= breathing_room) then
    ModSettingSet("water_world.breathing_room", next_value)
  end
  GuiLayoutEnd(gui)

  GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  GuiText(gui, 0, 0, "Scale the number of aquatic props and animals that spawn")
  GuiLayoutBeginHorizontal(gui, 0, 0)
  local aquatics_density = ModSettingGet("water_world.aquatics_density")
  GuiText(gui, 0, 0, "   Aquatic prop density:   ")
  if (aquatics_density == nil) then
    aquatics_density = 25
  end

  --GuiSlider( gui:obj, id:int, x:number, y:number, text:string, value:number, value_min:number, value_max:number, value_default:number, value_display_multiplier:number, value_formatting:string, width:number )
  local next_value = GuiSlider(gui, id(), -2, 0, "", aquatics_density, 0, 25, 5, 1, " $0 ", 65)
  if(next_value ~= aquatics_density) then
    ModSettingSet("water_world.aquatics_density", next_value)
  end
  GuiLayoutEnd(gui)

  GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  GuiText(gui, 0, 0, "Toggle spawning of segmented verlet chain kelp props")
  GuiLayoutBeginHorizontal(gui, 0, 0)
  local spawn_big_kelp = ModSettingGet("water_world.spawn_big_kelp")
  if not spawn_big_kelp then
    GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  end
  GuiText(gui, 0, 0, "   Spawn big kelp:   ")
  if (spawn_big_kelp == nil) then
    spawn_big_kelp = true
  end
  local text = spawn_big_kelp and GameTextGet("$option_on") or GameTextGet("$option_off")
  if not spawn_big_kelp then
    GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  end
  if GuiButton(gui, id(), 0, 0, text) then
    ModSettingSet("water_world.spawn_big_kelp", not spawn_big_kelp)
  end
  GuiLayoutEnd(gui)

  GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  GuiText(gui, 0, 0, "Holy mountains won't flood until they collapse unless this is disabled")
  GuiLayoutBeginHorizontal(gui, 0, 0)
  local protect_holy_mountains = ModSettingGet("water_world.protect_holy_mountains")
  if not protect_holy_mountains then
    GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  end
  GuiText(gui, 0, 0, "   Protect holy mountains:   ")
  if (protect_holy_mountains == nil) then
    protect_holy_mountains = true
  end
  local text = protect_holy_mountains and GameTextGet("$option_on") or GameTextGet("$option_off")
  if not protect_holy_mountains then
    GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  end
  if GuiButton(gui, id(), 0, 0, text) then
    ModSettingSet("water_world.protect_holy_mountains", not protect_holy_mountains)
  end
  GuiLayoutEnd(gui)

  GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  GuiText(gui, 0, 0, "Enable if you prefer pretending to be a dolphin over struggling to not drown")
  GuiLayoutBeginHorizontal(gui, 0, 0)
  local you_have_breathless = ModSettingGet("water_world.you_have_breathless")
  if not you_have_breathless then
    GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  end
  GuiText(gui, 0, 0, "   You have breathless:   ")
  if (you_have_breathless == nil) then
    you_have_breathless = false
  end
  local text = you_have_breathless and GameTextGet("$option_on") or GameTextGet("$option_off")
  if not you_have_breathless then
    GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  end
  if GuiButton(gui, id(), 0, 0, text) then
    ModSettingSet("water_world.you_have_breathless", not you_have_breathless)
  end
  GuiLayoutEnd(gui)

  GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  GuiText(gui, 0, 0, "Keep this enabled or all the cute little Hämis will drown")
  GuiLayoutBeginHorizontal(gui, 0, 0)
  local enemies_have_breathless = ModSettingGet("water_world.enemies_have_breathless")
  if not enemies_have_breathless then
    GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  end
  GuiText(gui, 0, 0, "   Enemies have breathless:   ")
  if (enemies_have_breathless == nil) then
    enemies_have_breathless = true
  end
  local text = enemies_have_breathless and GameTextGet("$option_on") or GameTextGet("$option_off")
  if not enemies_have_breathless then
    GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  end
  if GuiButton(gui, id(), 0, 0, text) then
    ModSettingSet("water_world.enemies_have_breathless", not enemies_have_breathless)
  end
  GuiLayoutEnd(gui)

  GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  GuiText(gui, 0, 0, "Spawn something helpful at the mountain entrance")
  GuiLayoutBeginHorizontal(gui, 0, 0)
  local helper_perks = ModSettingGet("water_world.helper_perks")
  if not helper_perks then
    GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  end
  GuiText(gui, 0, 0, "   Helper perks:   ")
  if (helper_perks == nil) then
    helper_perks = false
  end
  local text = helper_perks and GameTextGet("$option_on") or GameTextGet("$option_off")
  if not helper_perks then
    GuiColorSetForNextWidget(gui, 1, 1, 1, 0.5)
  end
  if GuiButton(gui, id(), 0, 0, text) then
    ModSettingSet("water_world.helper_perks", not helper_perks)
  end
  GuiLayoutEnd(gui)
end